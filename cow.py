class Cow:
    def __init__(self, name):
        self._name = name

    def say(self):
        return self._name + ": moooooooooo"
