from flask import Flask
from flask_cors import CORS

from cat import Cat
from dog import Dog

app = Flask(__name__)
CORS(app)


@app.route('/')
def hello():
    return "Hello world"


@app.route('/cat/<cat_name>')
def cat(cat_name):
    cat_obg = Cat(cat_name)
    return cat_obg.say()


@app.route('/dog/<dog_name>')
def dog(dog_name):
    dog_obg = Dog(dog_name)
    return dog_obg.say()


if __name__ == '__main__':
    app.run()
